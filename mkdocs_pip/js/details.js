// JavaScript to open the details element based on hash
function openParentDetails(element) {
  let parent = element.parentElement;
  while (parent) {
    if (parent.tagName.toLowerCase() === 'details') {
      parent.open = true;
    }
    parent = parent.parentElement;
  }
}

function openDetailsOnHash() {
  console.log("Hash change");
  const hash = window.location.hash;

  if (hash) {
    const detailsElement = document.querySelector(hash);

    if (detailsElement && detailsElement.tagName.toLowerCase() === 'details') {
      detailsElement.open = true;
    }

    if (detailsElement) {
      openParentDetails(detailsElement);
    }
  }
}

// Run the function on page load
openDetailsOnHash();

// Run the function on hash change
window.addEventListener('hashchange', openDetailsOnHash);
