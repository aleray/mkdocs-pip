document.addEventListener("DOMContentLoaded", function () {
    const links = document.querySelectorAll('p a[href^="http"]'); // Select external links only

    links.forEach(link => {
        const url = link.href;

        // Create the archive URL
        const archiveUrl = `http://archive.wikiwix.com/cache/?url=${encodeURIComponent(url)}`;

        // Create the archive link element
        const archiveLink = document.createElement('a');
        archiveLink.href = archiveUrl;
        archiveLink.className = "archived";
        archiveLink.textContent = " [archived]";

        // Append the archive link after the original link
        link.parentNode.insertBefore(archiveLink, link.nextSibling);
    });
});
