import html
import textwrap
import uuid

from customblocks.utils import E, Markdown
from datetime import datetime


def comment(ctx):
    return E(
        "small.sidenote",
        dict(),
        Markdown(ctx.content, ctx.parser),
    )


def persons(ctx):
    return E(
        "div.foo",
        dict(),
        Markdown(ctx.content, ctx.parser),
    )


def person(ctx, param1, param2):
    return E(
        "div.person",
        dict(),
        E(
            "div.person__image",
            dict(),
            E("img", src=param2),
        ),
        E(
            "div.person__about",
            dict(),
            E("h1.person__name", dict(), param1),
            E(
                "div.person__description",
                dict(),
                Markdown(ctx.content, ctx.parser),
            ),
        ),
    )


# def invidious_iframe(
#     ctx, id, *args, autoplay=False, controls=True, loop=False, style=None, **kwds
# ):
#     base_url = ctx.config.get("invidious_instance", "yewtu.be")
#     url = f"https://{base_url}/embed/{id}"
#     return E(
#         "".join(f".{cls}" for cls in ("embed-container", *args)),
#         E(
#             "iframe",
#             src=url,
#             frameborder=0,
#             allowfullscreen="true",
#             # style=iframeStyle,
#         ),
#         # style=wrapperStyle,
#         **kwds,
#     )


def invidious(
    ctx, id, *args, **kwds
):
    classes = list(args)

    base_url = ctx.config.get("invidious_instance", "yewtu.be")
    src = f"https://{base_url}/vi/{id}/maxres.jpg"
    url = f"https://{base_url}/watch?v={id}"
    original_url = f"https://www.youtube.com/watch?v={id}"
    original_link = f"<a href='{original_url}'>Or watch on Youtube</a>" 

    return E(
            "div.invidious",
            dict(
                _class = ' '.join(classes) or None,
            ),
            E(
                "a",
                dict(href=url, target="_blank"),
                E("img", src=src),
            ),
            E('figcaption',
                Markdown(ctx.content, parser=ctx.parser),
                original_link
            ),
        )


def sidebar(ctx, title=None, *args, **kwds):
    _id = kwds.pop('id', None) or (str(uuid.uuid4()))
    args = ["-".join(arg.split()) for arg in args]  # Untested case
    permalink = f'<a class="headerlink" href="#{_id}" title="Permanent link">¶</a>'
    return E(
            "div.sidebar",
            dict(),
            E(
                "details.sidebar__container",
                dict(id=_id, _class=" ".join(list(args))),
                E("summary.sidebar__title", (title or ctx.type.title()) + permalink),
                Markdown(ctx.content, ctx.parser),
                **kwds,
            )
        )


def section(ctx, title=None, *args, **kwds):
    args = ["-".join(arg.split()) for arg in args]  # Untested case
    return E(
        "details.section",
        dict(_class=" ".join(list(args))),
        E("summary.section__title", 
            E("h2", title or ctx.type.title()),
        ),
        Markdown(ctx.content, ctx.parser),
        **kwds,
    )


def ics(
    ctx, name=None, location=None, organizer=None, start=None, end=None, *args, **kwds
):
    date_format = "%Y-%m-%d %H:%M"
    dtstart = datetime.strptime(start, date_format).strftime("%Y%m%dT%H%M%SZ")
    dtend = datetime.strptime(end, date_format).strftime("%Y%m%dT%H%M%SZ")

    body = f"""
    BEGIN:VCALENDAR
    VERSION:2.0
    BEGIN:VEVENT
    SUMMARY:{name}
    DTSTART:{dtstart}
    DTEND:{dtend}
    DTSTAMP:20231211T130926Z
    UID:1702300166599-Test
    DESCRIPTION:{ctx.content}
    LOCATION:{location}
    ORGANIZER:{organizer}
    STATUS:CONFIRMED
    PRIORITY:0
    END:VEVENT
    END:VCALENDAR
    """

    data = html.escape(textwrap.dedent(body), quote=True)

    return E(
        "p.ics",
        dict(),
        E(
            "a",
            dict(
                href=f"data:text/calendar;charset=utf-8,{data}", download="exemple.ics"
            ),
            "Download ICS",
        ),
    )

def ref(
    ctx,
    url,
    *args,
    **kwds
):
    title = kwds.pop('title', url)
    id = kwds.pop('id', None)
    classes = list(args)

    return E('figure.ref',
        dict(
            _class = ' '.join(classes) or None,
            id = id,
        ),
        E('a',
            dict(
                href = url,
                target = '_blank'
            ),
            title,
        ),
        E('figcaption',
            Markdown(ctx.content, parser=ctx.parser)
        ),
        **kwds
    )
