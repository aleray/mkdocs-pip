import xml.etree.ElementTree as etree
import uuid


def make_elt(ns_typeof, typeof, ns_prop, prop, content, label):
    el = etree.Element("mark")

    if typeof:
        val = "{}:{}".format(ns_typeof, typeof)
        el.set("typeof", val)

    prop = "{}:{}".format(ns_prop, prop or "keywords")
    el.set("property", prop)
    el.set("content", content)

    # Generate a UUID and truncate it
    short_id = str(uuid.uuid4())[:8]  # Taking the first 8 characters
    el.set("id", short_id)
    el.text = label or content
    return el

