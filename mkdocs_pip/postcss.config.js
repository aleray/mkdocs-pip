module.exports = {
  plugins: {
    'stylefmt': {},
    'css-declaration-sorter': {
      order: 'smacss'
    },
    'postcss-single-line': {},
    // Add other plugins and their options if needed
  },
};
