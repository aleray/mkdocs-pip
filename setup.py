from setuptools import setup, find_packages

VERSION = '0.0.1'

setup(
    name="mkdocs-pip",
    version=VERSION,
    url='',
    license='',
    description='Mkdocs theme for Programmable Infrastructures project',
    author='Alexandre Leray',
    author_email='alexandre@stdin.fr',
    packages=find_packages(),
    py_modules=['semanticdata', 'generators'],
    include_package_data=True,
    entry_points={
        'mkdocs.themes': [
            'pip = mkdocs_pip',
        ]
    },
    zip_safe=False
)
